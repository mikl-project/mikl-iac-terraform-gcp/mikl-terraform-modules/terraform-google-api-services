variable "project" {
type = string
}

variable "service" {
type = list(string)
default = [
    "cloudresourcemanager.googleapis.com",
    "serviceusage.googleapis.com"
  ]
}

variable "depends_on" {

}


