resource "google_project_service" "api-module" {
  project = var.project
  depends_on = [var.depends-on]
  for_each = toset(var.service)
  service = each.key
  lifecycle {
    prevent_destroy = true
  }
}
